#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include "include_c/libxl.h"

pid_t fork(void);
pid_t wait(int *stat_loc);

pid_t child_pid, wpid;
int status = 0;

const int max_len=5;
const char ascii[] = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!#$%&'()*+.-/:;<=>?@[]^_{|}~\"§öäü€ßÄÖÜ";
//const char ascii[] = "abc";


void variant (int novar)
{
    int var[novar];
    int i,lenstring;
    double count;
    lenstring=strlen(ascii);
    count=0;

    for (i=0;i<=novar;++i)
    {
        var[i]=0; // initalize arra
    }
    i=novar-1;
    while(i >= 0)
    {
        i=novar-1; // start from the right
        while (var[i] < lenstring)
        {
            //printf("i: %d Ascii %c\n",i,ascii[var[i]]);
//             for (j=0;j<novar;++j)
//             {
//                 printf("%c",ascii[var[j]]);
//             }
//             printf("\n");
            ++count;
            ++var[i];
        }
LOOP:
        --i;
        if (i >= 0)
        {
            var[i+1]=0;
            ++var[i];
            if (var[i] >= lenstring) // increase next
            {
                goto LOOP;
            }
           // printf("i: %d Ascii %c\n",i,ascii[var[i]]);
        }
    }
    printf("novar: %d strlen(ascii) %d Count: %f\n",novar,lenstring,count);
}

void excel()
{

    BookHandle book = xlCreateBook();
    if(book)
    {
        if(xlBookLoad(book, "example.xls"))
        {
            SheetHandle sheet = xlBookGetSheet(book, 0);
            if(sheet)
            {
                double d = xlSheetReadNum(sheet, 3, 1, 0);
                xlSheetWriteNum(sheet, 3, 1, d * 2, 0);
                xlSheetWriteStr(sheet, 4, 1, "new string", 0);
            }

            if(xlBookSave(book, "example.xls")) printf("File example.xls has been modified.\n");
        }

        xlBookRelease(book);
    }

}


int main(int argc, char *argv[])
{

    //excel();
    //return 0;

    for (int id=1; id<max_len; id++) {
        if ((child_pid = fork()) == 0)
        { // Child-Code
            printf("%d ------ Start -------------------------\n",id);
            variant(id);
            printf("-------- Finish ----------------------- %d\n",id);
            exit(0);
        }
    }

    while ((wpid = wait(&status)) > 0);



    return 0;
}
